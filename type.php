<?php
include './include/common.inc.php';
if(!$id || !is_numeric($id)){
    /*
     * 若没有ID参数，则默认显示所有分类页面
     */
    $q1=$db->query("select * from types where pid = 0");
    $types=array();
    $i=0;
    while($row1=$db->fetch_array($q1)){
        $types[$i]['pid']=$row1;
        $q2=$db->query("select * from types where pid = {$row1['id']}");
        while($row2=$db->fetch_array($q2)){
            $types[$i]['cid'][]=$row2;
        }
        $i++;
    }
    $tpl->assign("types",$types);
    $tpl->assign("title","所有分类-");
    $tpl->display("type_all.html");
}else{
    /*
     * 获取该分类信息
     *
     */
    $typeinfo=$db->fetch_first("select *,concat(path,'-',{$id}) as bpath from types where id = {$id}");
    $tpl->assign("typeinfo",$typeinfo);
    /*
     * 获取子类别
     */
    $q=$db->query("select * from types where pid = {$id}");
    $cids=array();
    while($row=$db->fetch_array($q)){
        $cids[]=$row;
    }
    $tpl->assign("cids",$cids);
    /*
     * 父分类标题
     */        
    $typepaths = array();
    $typepaths = explode('-', $typeinfo['path']);
    $typeparents = array();
    $pids=array();
    foreach ($typepaths as $v) {
        if ($v != 0) {
            $q = $db->fetch_first("select * from types where id = {$v} limit 1");
            $typeparents[] = $q['name'];
            $pids[$v]=$q['name'];
            unset($q);
        }
    }
    krsort($typeparents);
    $pidstr=implode('-', $typeparents);
    $pidstr=$pidstr?'-'.$pidstr:'';
    $title = $typeinfo['name'].$pidstr.'-';
    $tpl->assign("title",$title);
    ksort($pids);
    $tpl->assign("pids",$pids);
    /*
     * 读取商品
     */
    $cids_1=array();
    $q=$db->query("select id from types where path like '{$typeinfo['bpath']}%'");
    while($row = $db->fetch_array($q)){
        $cids_1[]=$row['id'];
    }
    $cids_s=implode(',',$cids_1);
    $cids_s=$cids_s?$id.','.$cids_s:$id;
    $cids_s="(".$cids_s.")";
    
    
    $perpage = 12;
    $pagevar = "page";
    $nowtime = time();
    $q2 = $db->query("select id from commo where typeid={$id}");

    $pages = new Fpage();
    $pages->totalNums = $db->num_rows($q2);
    $pages->perpageNum = $perpage;
    $pages->pageVar = $pagevar;
    $pages->jump_pageinputId = "jumppage";
    $pagestr = $pages->showpages();
    $page = ($$pagevar && is_numeric($$pagevar)) ? $$pagevar : 0;
    $leftstart = $perpage * (max(0, $page - 1));
    $q3 = $db->query("select  commo.*,attachment.path,attachment.isimg,types.name as typename from commo LEFT JOIN types on commo.typeid=types.id LEFT JOIN attachment on attachment.commoid=commo.id where commo.typeid in {$cids_s} group by commo.id order by commo.id desc limit {$leftstart},{$perpage}");
    $commolist = array();
    while ($row = $db->fetch_array($q3)) {
        $commolist[] = $row;
    }
    unset($q3);
    $tpl->assign("commolist", $commolist);
    $tpl->assign("page", $pagestr);
    /*
     * 热门商品
     */
    $hotsell=array();
    $q4=$db->query("select commo.*,types.name as typename from commo,types where commo.typeid in {$cids_s} and types.id=commo.typeid order by sell desc limit 0,10");
    while ($row4=$db->fetch_array($q4)){
        $hotsell[]=$row4;
    }
    $tpl->assign("hotsell",$hotsell);
    /*
     * 促销商品 discount>0
     */
    $cheap_commo=array();
    $q5=$db->query("select commo.*,types.name as typename from commo,types where commo.typeid in {$cids_s} and types.id=commo.typeid and commo.discount>0 order by id desc limit 0,10");
    while ($row5=$db->fetch_array($q5)){
        $cheap_commo[]=$row5;
    }
    $tpl->assign("cheap_commo",$cheap_commo);
    
    $tpl->display("type.html");
}

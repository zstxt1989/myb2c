<?php
include './include/common.inc.php';
if(!$id || !is_numeric($id)){
    header("HTTP/1.1 404 Not Found");
    $tpl->display("404.html");
    exit;
}
$commo_info=$db->fetch_first("select * from commo where id = {$id} limit 1");
if(!$commo_info){
    $help->showmessage("该商品不存在！",-1);
}
$type=$db->fetch_first("select * from types where id={$commo_info['typeid']} limit 1");
$commo_info['typename']=$type['name'];

/*
 * 处理父分类
 */
$typepaths=array();
$typepaths=explode('-',$type['path']);
$typeparents=array();
foreach ($typepaths as $v){
    if($v!=0){
        $q=$db->fetch_first("select * from types where id = {$v} limit 1");
        $typeparents[$v]=$q['name'];
        unset($q);
    }
}
ksort($typeparents);
$commo_info['typeparents']=$typeparents;

/*
 * title模板变量
 */
$title=$commo_info['name'].'-'.$commo_info['seoname'].'-'.$commo_info['typename'].'-';
$tpl->assign("title",$title);

/*
 * 将商品信息代入模板
 */
$tpl->assign("commo_info",$commo_info);
/*
 * 获取图片附件，4个
 */
$q=$db->query("select aid from attachment where commoid={$id} and isimg=1");
$commo_pics=array();
while($row=$db->fetch_array($q)){
    $commo_pics[]=$C['SITE_URL'].'/img.php?aid='.$row['aid'];
}
unset($q);
if(count($commo_pics)==0){
    $commo_pics[]=$C['SITE_URL']."/images/nopic.jpg";
}
$tpl->assign("commo_pics",$commo_pics);

/*
 * 头部额外信息，加载图片放大镜
 */
$headext="
<script src='{$C['SITE_URL']}/include/js/jqzoom/jqzoom.js'></script>
<link rel='stylesheet' type='text/css' href='{$C['SITE_URL']}/include/js/jqzoom/jqzoom.css' />

";
$tpl->assign("headext",$headext);
/*
 * 当前分类下热卖商品
 */
$typehotsell=array();
$q=$db->query("select * from commo where typeid={$commo_info['typeid']} and id!={$id} order by sell desc limit 0,10");
while($row=$db->fetch_array($q)){
    $typehotsell[]=$row;
}
unset($q);
$tpl->assign("typehotsell",$typehotsell);
/*
 * 当前分类最新商品
 */
$typenew=array();
$q=$db->query("select * from commo where typeid={$commo_info['typeid']} and id!={$id} order by addtime desc limit 0,10");
while($row=$db->fetch_array($q)){
    $typenew[]=$row;
}
unset($q);       
$tpl->assign("typenew",$typenew);








$tpl->display("commo.html");



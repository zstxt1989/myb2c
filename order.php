<?php
include './include/common.inc.php';
if (!$U['uid']) {
        $help->showmessage("请先登陆");
}
if(!$_POST['paysubmit']){
    
    if(!is_numeric($id)){
        $help->showmessage("非法参数");
    }
    $q=$db->fetch_first("select * from `order` where id = {$id}");
    if(!$q){
        $help->showmessage("订单不存在");
    }
    if($q['uid']!=$U['uid']){
        $help->showmessage("订单不属于你，无权操作");
    }
    $orderinfo=array();
    $items  =  unserialize($q['item']);
    foreach($items as $v){
        $q1=$db->fetch_first("select id,name from commo where id = {$v['id']}");
        $orderinfo['item'].='<a href='.$C['SITE_URL'].'/commo.php?id='.$q1['id'].'><b>'.$q1['name'].'</b></a> <font color=green>x '.$v['count'].'件</font><br>';
        unset($q1);
    }
    $orderinfo['id']=$q['id'];
    $orderinfo['price']=$q['price'];
    $orderinfo['status']=$q['status'];
    $orderinfo['time']=$q['time'];
    $orderinfo['paytime']=$q['paytime'];
    $orderinfo['address']=$q['address'];
    $tpl->assign("orderinfo",$orderinfo);
    $tpl->display("order.html");
}else{
    if(!$orderid){
        $help->showmessage("未捕获到订单");
        
    }
    if(!is_numeric($tel)){
        $help->showmessage("请输入合法的联系电话",-1);
    }
    if(strlen($address)<5){
        $help->showmessage("未填写收货地址或太短，请返回认真填写",-1);
    }
    
    $address="联系电话：".$tel." 收货地址：".$address;
    
    $q=$db->fetch_first("select price from `order` where id = {$orderid}");
    if(!$q){
        $help->showmessage("订单不存在，或被删除",-1);
    }
    $q1=$db->fetch_first("select money from member where uid = {$U['uid']}");
    if(!$q1){
        $help->showmessage("请先登陆合法账户");
    }
    if(($q1['money']-$q['price'])<0){
        $help->showmessage("账户余额不足，请先充值");
    }
    $nowtime=time();
    $db->query("update member set money=money-{$q['price']} where uid = {$U['uid']}");
    $db->query("update `order` set address='{$address}',status=1,paytime={$nowtime} where id = {$orderid}");
    header('Location:'.$C['SITE_URL'].'/order.php?id='.$orderid);
}

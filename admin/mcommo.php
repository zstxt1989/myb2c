<?php
include '../include/common.inc.php';
$adminhelp=new Adminhelp();
if(!$q=$adminhelp->mcheckol()){
    header("Location:".$C['SITE_URL']."/admin/login.php");
    exit;
}
$adminhelp->mupdatesession();
unset($q);
if(!$action){
    $perpage=10;
    $pagevar="page";
    $q2=$db->query("select id from commo");
    
    $pages=new Fpage();
    $pages->totalNums=$db->num_rows($q2);
    $pages->perpageNum=$perpage;
    $pages->pageVar=$pagevar;
    $pages->jump_pageinputId="jumppage";
    $pagestr=$pages->showpages();
    $page=($$pagevar && is_numeric($$pagevar))?$$pagevar:0;
    $leftstart=$perpage*(max(0,$page-1));
    $q3=$db->query("select c.*,t.name as typename from commo c,types t where c.typeid=t.id order by c.id desc limit {$leftstart},{$perpage} ");
    $commolist=array();
    while($row=$db->fetch_array($q3)){       
        $commolist[]=$row;
    }
    unset($q3);
    $tpl->assign("commolist",$commolist);
    $tpl->assign("page",$pagestr);
    
    
    
    $tpl->display("admin/mcommo.html");
}elseif($action==add){
    if(!$help->submitcheck()){
        $typelist=$M->gettypeoptions();                   
        $tpl->assign("options",$typelist);
        $tpl->display("admin/mcommo_add.html");
        exit;
    }
    if(!$typeid || !is_numeric($typeid)){
        exit("错误：未选择分类");
    }
    //商品信息入库
    $addtime=time();
    $price=round($price,2);
    $discount=(int)$discount>9?9:((int)$discount<0?0:(int)$discount);
    $db->query("insert into commo (name,seoname,info,typeid,price,vprice,stock,addtime,discount) values ('{$name}','{$seoname}','{$info}',{$typeid},'{$price}','{$vprice}',{$stock},{$addtime},{$discount})");
    //处理附件
    $insertid=$db->insert_id();
    $msg="添加成功!";
    $att = is_array($att)?$att:array();
    foreach($att as $v){
        $q=$db->fetch_first("select * from attachment where aid = {$v}");
        if($q['isimg']==1 && $q['commoid']==0){
            if(file_exists(ROOT.$C['attpath'].$q['path'])){
                $filename =  basename(ROOT.$C['attpath'].$q['path']);
                $rename=rename(ROOT.$C['attpath'].$q['path'],ROOT.$C['attpath'].'images/'.$filename);
                if($rename){
                    $path='images/'.$filename;
                    $db->query("update attachment set commoid={$insertid},typeid={$typeid},path='{$path}' where aid={$v}");
                    
                }else{
                    $msg.="<br>移动文件".ROOT.$C['attpath'].$q['path']."失败";
                }
            }else{
                $msg.="<br>文件".ROOT.$C['attpath'].$q['path']."不存在";
            }
        }
    }
    
        $help->showmessage($msg,-1,3);
    
    
    
}elseif($action=="edit"){    
    if($help->submitcheck()){
        if(!isset($commoid)){
            exit("未指定商品ID");
        }
        
        if(!isset($typeid) || !is_numeric($typeid)){
            exit("分类ID非法");
        }
        $msg="更新商品信息成功";
        /*
         * 处理图片附件
         */
        if(isset($att) && is_array($att)){
            foreach($att as $aid){
                if(!is_numeric($aid)){
                    exit("附件aid非法");
                }
                $q=$db->fetch_first("select * from attachment where aid = {$aid}");
                if($q && $q['isimg']==1 && $q['commoid']==0){
                    if(file_exists(ROOT.$C['attpath'].$q['path'])){
                        $filename =  basename(ROOT.$C['attpath'].$q['path']);
                        $rename=rename(ROOT.$C['attpath'].$q['path'],ROOT.$C['attpath'].'images/'.$filename);
                        if($rename){
                            $path='images/'.$filename;
                            $db->query("update attachment set commoid={$commoid},typeid={$typeid},path='{$path}' where aid={$aid}");

                        }else{
                            $msg.="<br>移动文件".ROOT.$C['attpath'].$q['path']."失败";
                        }
                    }else{
                        $msg.="<br>文件".ROOT.$C['attpath'].$q['path']."不存在";
                    }
                }
            }
        }
        /*
         * 处理表单信息
         * 
         */
        $discount=(int)$discount>9?9:((int)$discount<0?0:(int)$discount);
        $q=$db->query("update commo set name='{$name}',seoname='{$seoname}',info='{$info}',typeid={$typeid},price={$price},vprice={$vprice},stock={$stock},sell={$sell},discount={$discount} where id = {$commoid}");
        if(!$q){
            exit("更新商品信息失败，错误位置：更新商品表信息的时候");
        }
        $help->showmessage($msg,-1,3);
    }else{
        if(!isset($commoid)){
        exit("未指定商品ID");
        }
        $commoid  =  is_numeric($commoid)?$commoid:(int)$commoid;
        $q=$db->fetch_first("select * from commo where id = {$commoid}");
        if(!$q){        
            exit("未找到指定商品，或商品ID不合法");
        }
        $commoinfo=$q;
        unset($q);
        $tpl->assign("commoinfo",$commoinfo);
        $typelist=$M->gettypeoptions();                   
        $tpl->assign("options",$typelist);
        $tpl->display("admin/mcommo_edit.html");
    }
    
}elseif($action=="del"){
    if(!is_numeric($commoid)){
        exit("非法ID");
    }
    if($M->delcommo($commoid)){
        $help->showmessage("删除成功",$C['SITE_URL']."/admin/mcommo.php",5);
    }else{
        exit("删除失败");
    }
    
}elseif($action == "discount"){
    $perpage=10;
    $pagevar="page";
    $q2=$db->query("select id from commo where discount > 0");
    
    $pages=new Fpage();
    $pages->totalNums=$db->num_rows($q2);
    $pages->perpageNum=$perpage;
    $pages->pageVar=$pagevar;
    $pages->jump_pageinputId="jumppage";
    $pagestr=$pages->showpages();
    $page=($$pagevar && is_numeric($$pagevar))?$$pagevar:0;
    $leftstart=$perpage*(max(0,$page-1));
    $q3=$db->query("select c.*,t.name as typename from commo c,types t where c.discount >0 and  c.typeid=t.id order by c.id desc limit {$leftstart},{$perpage} ");
    $commolist=array();
    while($row=$db->fetch_array($q3)){       
        $commolist[]=$row;
    }
    unset($q3);
    $tpl->assign("commolist",$commolist);
    $tpl->assign("page",$pagestr);
    
    
    
    $tpl->display("admin/mcommo_discount.html");
}else{
    header("http/1.1 404 Not Found"); 
    exit;
}

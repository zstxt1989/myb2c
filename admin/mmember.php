<?php
include '../include/common.inc.php';
$adminhelp=new Adminhelp();
if(!$q=$adminhelp->mcheckol()){
    header("Location:".$C['SITE_URL']."/admin/login.php");
    exit;
}
$adminhelp->mupdatesession();
if(!$action){
    $perpage=10;
    $pagevar="page";
    $q2=$db->query("select uid from member");
    
    $pages=new Fpage();
    $pages->totalNums=$db->num_rows($q2);
    $pages->perpageNum=$perpage;
    $pages->pageVar=$pagevar;
    $pages->jump_pageinputId="jumppage";
    $pagestr=$pages->showpages();
    $page=($$pagevar && is_numeric($$pagevar))?$$pagevar:0;
    $leftstart=$perpage*(max(0,$page-1));
    $q3=$db->query("select * from member order by uid desc limit {$leftstart},{$perpage} ");
    $memlist=array();
    while($row=$db->fetch_array($q3)){       
        $memlist[]=$row;
    }
    unset($q3);
    $tpl->assign("memlist",$memlist);
    $tpl->assign("page",$pagestr);
    $tpl->display("admin/mmember.html");
}elseif($action=="add"){
    if($submit){
        $err=array();
        if(!$_POST['name']){
            $err['name']="用户名不能为空";
        }elseif($checkstr=$help->checksth($_POST['name'],'safe')){
            if($checkstr){
                $err['name']="用户名".$checkstr;
            }
            unset($checkstr);
        }elseif($help->utf8_strlen($_POST['name'])>15 || $help->utf8_strlen($_POST['name'])<6 ){
            $err['name']="用户名太长或太短,请保持6-15位长度";
        }elseif($q1=$db->fetch_first("select name from member where uid!={$uid} and name = '".trim($_POST['name'])."'")){
            if($q1){
                $err['name']="用户名已存在";
                
            }
            unset($q1);
        }
        if(!$_POST['pw']){
            $err['pw']="密码不能为空";
        }
        if(!$_POST['ask1']){
            $err['ask1']="提示问题必须填写！用于找回密码！";
        }elseif(preg_match("/[\'\"]/",$_POST['ask1'])){
            $err['ask1']="不得包含单引号和双引号！";
        }
        if(!$_POST['ask2']){
            $err['ask2']="提示问题必须填写！用于找回密码！";
        }elseif(preg_match("/[\'\"]/",$_POST['ask1'])){
            $err['ask2']="不得包含单引号和双引号！";
        }
        if(!$_POST['ans1']){
            $err['ans1']="答案必须填写！和提示问题搭配用于找回密码！";
        }elseif(preg_match("/[\'\"]/",$_POST['ask1'])){
            $err['ans1']="不得包含单引号和双引号！";
        }
        if(!$_POST['ans2']){
            $err['ans2']="答案必须填写！和提示问题搭配用于找回密码！";
        }elseif(preg_match("/[\'\"]/",$_POST['ask1'])){
            $err['ans2']="不得包含单引号和双引号！";
        }
        
        if(empty($err)){
            $name=trim($_POST['name']);
            $pw=md5($_POST['pw']);
            $ask1=$_POST['ask1'];
            $ask2=$_POST['ask2'];
            $ans1=$_POST['ans1'];
            $ans2=$_POST['ans2'];
            $regtime=time();
            
            $db->query("insert into member (name,pw,regtime,money,extcredits1,ask1,ans1,ask2,ans2) values ('{$name}','{$pw}',{$regtime},0,0,'{$ask1}','{$ans1}','{$ask2}','{$ans2}')");
            $help->showmessage("添加成功",$C['SITE_URL'].'/admin/mmember.php',3);
        }else{
            $tpl->assign("err",$err);
        }
    }
    $tpl->display("admin/mmember_add.html");
}elseif($action=="edit"){
    if(!$uid || !is_numeric($uid)){
        exit("用户ID未指定或非法");
    }
    $q=$db->fetch_first("select * from member where uid = {$uid}");
    if(!$q){
        exit("用户不存在");
    }
    if($submit){
        $err=array();
        
        if(!$_POST['name']){
            $err['name']="用户名不能为空";
        }elseif($checkstr=$help->checksth($_POST['name'],'safe')){
            if($checkstr){
                $err['name']="用户名".$checkstr;
            }
            unset($checkstr);
        }elseif($help->utf8_strlen($_POST['name'])>15 || $help->utf8_strlen($_POST['name'])<6 ){
            $err['name']="用户名太长或太短,请保持6-15位长度";
        }elseif($q1=$db->fetch_first("select name from member where uid!={$uid} and name = '".trim($_POST['name'])."'")){
            if($q1){
                $err['name']="用户名已存在";
                $q['name']=$_POST['name'];
            }
            unset($q1);
        }
        if(!$_POST['pw']){
            $err['pw']="密码不能为空";
        }
        if(!$_POST['ask1']){
            $err['ask1']="提示问题必须填写！用于找回密码！";
        }elseif(preg_match("/[\'\"]/",$_POST['ask1'])){
            $err['ask1']="不得包含单引号和双引号！";
        }
        if(!$_POST['ask2']){
            $err['ask2']="提示问题必须填写！用于找回密码！";
        }elseif(preg_match("/[\'\"]/",$_POST['ask1'])){
            $err['ask2']="不得包含单引号和双引号！";
        }
        if(!$_POST['ans1']){
            $err['ans1']="答案必须填写！和提示问题搭配用于找回密码！";
        }elseif(preg_match("/[\'\"]/",$_POST['ask1'])){
            $err['ans1']="不得包含单引号和双引号！";
        }
        if(!$_POST['ans2']){
            $err['ans2']="答案必须填写！和提示问题搭配用于找回密码！";
        }elseif(preg_match("/[\'\"]/",$_POST['ask1'])){
            $err['ans2']="不得包含单引号和双引号！";
        }
        
        if(empty($err)){
            $name=trim($_POST['name']);
            $pw=md5($_POST['pw']);
            $ask1=$_POST['ask1'];
            $ask2=$_POST['ask2'];
            $ans1=$_POST['ans1'];
            $ans2=$_POST['ans2'];
            
            $money=$_POST['money']?(int)$_POST['money']:0;
            $extcredits1=$_POST['extcredits1']?(int)$_POST['extcredits1']:0;
            $status=$_POST['status']?((int)$_POST['status']>0?1:0):0;
            $db->query("update member set name='{$name}',pw='{$pw}',ask1='{$ask1}',ans1='{$ans1}',ask2='{$ask2}',ans2='{$ans2}',money='{$money}',extcredits1='{$extcredits1}',status={$status} where uid={$uid}");
            $help->showmessage("修改成功",$C['SITE_URL'].'/admin/mmember.php',3);
        }else{
            $tpl->assign("err",$err);
        }
        
    }
    
    
    
    
    $tpl->assign("meminfo",$q);
    $tpl->display("admin/mmember_edit.html");
}elseif($action=="delete"){
    if(!$uid || !is_numeric($uid)){
        exit("用户ID未指定或非法");
    }
    $q=$db->fetch_first("select * from member where uid = {$uid}");
    if(!$q){
        exit("用户不存在");
    }
    /*
     * 删除订单信息，保留
     */
    
    
    /*
     * 删除用户信息
     */
    $db->query("delete from member where uid = {$uid}");
    $help->showmessage("删除成功",$C['SITE_URL'].'/admin/mmember.php',3);
}
<?php
include '../include/common.inc.php';
$adminhelp=new Adminhelp();
if(!$q=$adminhelp->mcheckol()){
    header("Location:".$C['SITE_URL']."/admin/login.php");
    exit;
}
$adminhelp->mupdatesession();
if(!$action){
    $perpage=10;
    $pagevar="page";
    $q2=$db->query("select id from types");
    
    $pages=new Fpage();
    $pages->totalNums=$db->num_rows($q2);
    $pages->perpageNum=$perpage;
    $pages->pageVar=$pagevar;
    $pages->jump_pageinputId="jumppage";
    $pagestr=$pages->showpages();
    $page=($$pagevar && is_numeric($$pagevar))?$$pagevar:0;
    $leftstart=$perpage*(max(0,$page-1));
    $q3=$db->query("select id,name,pid,path,concat(path,'-',id) as dpath from types order by dpath limit {$leftstart},{$perpage} ");
    $typelist=array();
    while($row=$db->fetch_array($q3)){
        $count=count(explode('-',$row['path']));
        $row['n']="|";
        for($i=0;$i<$count*4;$i++){
            $row['n'].='-';
        }
        $row['n'].='|';
        $typelist[]=$row;
    }
    unset($q3);
    $tpl->assign("typelist",$typelist);
    $tpl->assign("page",$pagestr);
    //$tpl->assign("config",$C);
    $tpl->display("admin/mtype.html");
}elseif($action=="add"){
    if($help->submitcheck()){
       
       $pid=$pid?(int)$pid:0;
       $q2=$db->fetch_first("select path from types where id={$pid} limit 0,1");
       if($q2){
           $path=$q2['path'].'-'.$pid;
       }else{
           $path='0';
       }
       unset($q2);
       $db->query("insert into types (name,seotitle,des,pid,path) values ('{$typename}','{$seotitle}','{$des}','{$pid}','{$path}')");
       
       $help->showmessage("添加成功",-1,3); 
    }else{
        $typelist=$M->gettypeoptions();   
        
        
        $tpl->assign("options",$typelist);
        
        //$tpl->assign("config",$C);
        $tpl->display("admin/mtype_add.html");
    }
}elseif($action=="edit"){
    if($help->submitcheck()){
        $id=isset($id)?(int)$id:0;
        $pid=$pid?(int)$pid:0;
        $q6=$db->fetch_first("select path from types where id={$pid} limit 0,1");
        if($q6){
            $path=$q6['path'].'-'.$pid;
        }else{
            $path='0';
        }
        unset($q6);
        $db->query("update types set name='{$typename}',seotitle='{$seotitle}',des='{$des}',pid='{$pid}',path='{$path}' where id={$id}");       
        $help->showmessage("修改成功",-1,3);
    }
    if(!isset($typeid)){
        $help->showmessage("非法操作",-1,3);
    }
    $typeid=(int)$typeid;
    $q5=$db->fetch_first("select * from types where id = {$typeid} limit 0,1");
    if($q5){
        $typelist=$M->gettypeoptions();                  
        $tpl->assign("options",$typelist);
        $tpl->assign("typeinfo",$q5);
        $tpl->display("admin/mtype_edit.html");
    }else{
        showmessage("该分类不存在",-1,3);
    }
}elseif($action=="delete"){
    if(!isset($typeid)){
        $help->showmessage("非法操作",-1,3);
    }
    $typeid=(int)$typeid;
    $q4=$db->fetch_first("select concat(path,'-',id) as dpath from types where id={$typeid}");
//    if($q4){
//        $db->query("delete from types where path like '{$q4['dpath']}%'");
//        $db->query("delete from types where id={$typeid}");
//    }
    
    
    $q=$db->query("select id from types where id={$typeid} or (path like '{$q4['dpath']}%')");
    $typeids=array();
    while($row = $db->fetch_array($q)){
        $typeids[]=$row['id'];
    }
    
    if($M->deltypes($typeids)){
        $help->showmessage("删除成功",-1,1);
    }else{
        exit("删除失败");
    }
}


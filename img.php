<?php
include 'include/common.inc.php';
$aid=$_GET['aid']?(int)$_GET['aid']:0;
if(!$aid){
    exit;
}
$q=$db->fetch_first("select * from attachment where aid = {$aid}");
if($q && $q['isimg']==1){
    header('Content-Disposition: inline; filename='.$q['filename'].'.thumb.jpg');
    header("Content-Type:image/pjpeg");
    $content =  file_get_contents(ROOT.$C['attpath'].$q['path']);
    echo $content;   
}else{
    exit;
}
    
<?php
/*
 * smarty 公共标签或函数
 */


/*
 * 获取指定分类
 * $pid 父类ID，默认为0获取所有顶级分类，若指定父类ID，则获取其子分类
 */
function gettypes($pid=0){
    global $db;
    
        $q=$db->query("select * from types where pid={$pid}");
        $row=array();
        while($r = $db->fetch_array($q)){
            $row[]=$r;
        }
        return $row;
    
    
}
$tpl->registerPlugin("function","gettypes","gettypes");

/*
 * 获取所有分类，返回一个数组
 */
function getalltypes(){
        global $db;
    
        $q=$db->query("select * from types");
        $row=array();
        while($r = $db->fetch_array($q)){
            $row[]=$r;
        }
        return $row;
}
/*
 * 获取商品列表,返回数组
 */        
function getcommo($num=5,$orderkey="id",$ordertype="DESC",$start=0){
    global $db,$C;
    $q=$db->query("select * from commo order by {$orderkey} {$ordertype} limit {$start},{$num}");
    $arr=array();
    while($row=$db->fetch_array($q)){       
        $q1=$db->fetch_first("select path from attachment where commoid={$row['id']} and isimg=1 limit 0,1");
        if($q1){
            $row['img']=$C['SITE_URL'].'/'.$C['attpath'].$q1['path'];
        }else{
            $row['img']=$C['SITE_URL'].'/images/nopic.jpg';
        }
        $arr[]=$row;
    }
    return $arr;
}
/*
 * 获取打折商品列表
 */
function getdiscount($num=5){
    global $db,$C;
    $q=$db->query("select * from commo where discount > 0 order by id desc limit 0,{$num}");
    $arr=array();
    while($row=$db->fetch_array($q)){       
        $q1=$db->fetch_first("select path from attachment where commoid={$row['id']} and isimg=1 limit 0,1");
        if($q1){
            $row['img']=$C['SITE_URL'].'/'.$C['attpath'].$q1['path'];
        }else{
            $row['img']=$C['SITE_URL'].'/images/nopic.jpg';
        }
        $arr[]=$row;
    }
    return $arr;
}
/*
 * 友情链接列表
 */
function getflinks(){
    global $db;
    $q=$db->query("select * from flink order by id desc");
    $arr=array();
    while($row=$db->fetch_array($q)){
        $arr[]=$row;
    }
    return $arr;
}

$tpl->assign("flinks",getflinks());
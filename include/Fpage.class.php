<?php
/*
 * 使用说明
 * 1.创建对象
 * 2.获取总记录数，并传值给$totalNums
 * 3.设置每页显示数目，$perpageNum
 * 可选设置：
 * $pageVar  传递页码的参数名称，默认为page
 * $jump_pageinputId;//跳转表单的ID，默认为page_jump
 * $pageNums 分页显示的页码数量，默认为10个
 * 示例：
 *$d=new fpage();
 *$d->totalNums=100;
 *$d->perpageNum=3;
 *$d->pageVar="fpage";
 *$d->jump_pageinputId="jpage";   
 *echo $d->showpages();
 */
class Fpage {
    //put your code here
    private $page;//当前页
    private $pageVar;//页码字段名    
    private $jump_pageinputId;//跳转表单的ID
    private $pageNums=10;//显示页码数
    private $totalNums;
    private $perpageNum;
    private $pages;//总页数
    function __set($arg,$value){
        if($arg=="pageVar"||$arg=="totalNums"||$arg=="perpageNum"||$arg=="jump_pageinputId"||$arg=="pageNums")
            $this->$arg=$value;
        else
            return NULL;
    }
    function __get($arg){
        if($arg=="page"||$arg=="perpageNum")
            return $this->$arg;
        else 
            return NULL;
        
    }
    
    //显示分页
    function showpages(){
        $this->pageVar=isset($this->pageVar)?$this->pageVar:"page";
        
        $this->jump_pageinputId=isset($this->jump_pageinputId)?$this->jump_pageinputId:"page_jump";
        $this->page=isset($_REQUEST[$this->pageVar])?$_REQUEST[$this->pageVar]:1;
        
        if(!isset($this->totalNums)){
            die("未获取到总记录数");
        }elseif(!isset ($this->perpageNum)){
            die("未设置每页显示数量");
        }
        $this->pages=ceil($this->totalNums/$this->perpageNum);
        $a=min($this->page,$this->pages);
        $start=($a-4)>0?($a-4):1;
        $end=min($start+$this->pageNums-1,$this->pages);
        $pageStr="<a href='{$this->_geturl(1)}'>首页</a> ";
        if($this->page>1){
            $pageStr.="<a href='{$this->_geturl(max(1,$this->page-1))}'>上一页</a>";
        }
        for($i=$start;$i<=$end;$i++){
            if($i==$this->page){
                $pageStr.="<<a href=\"".$this->_geturl($i)."\"><font color=red>$i</font></a>>";
            }else{
                $pageStr.="<<a href=\"".$this->_geturl($i)."\">$i</a>>";
            }
        }
        if($this->page<$this->pages){
            $pageStr.="<a href='{$this->_geturl(min($this->pages,$this->page+1))}'>下一页</a>";
        }
        $pageStr.=" <a href='{$this->_geturl($this->pages)}'>末页</a> ";
        $pageStr.=" 当前第$this->page / $this->pages 页"." <a onclick=\"javascript:window.location.href='{$this->_geturl()}'+document.getElementById('{$this->jump_pageinputId}').value;\" href=#>跳转>></a><input type=text id={$this->jump_pageinputId} style='width:50px;height:20px;'>";
        
        return $pageStr;
    }
    private function _geturl($i=""){
            $url="";
                  
            $parse=parse_url($_SERVER["REQUEST_URI"]);
            if(isset($parse["query"])){
                parse_str($parse["query"],$params);
                unset($params[$this->pageVar]);
                $params[$this->pageVar]=$i;
                $url=$parse['path'].'?'.http_build_query($params);
            }else{
                $url=$_SERVER["REQUEST_URI"].(strpos($_SERVER["REQUEST_URI"],'?')?'':'?').$this->pageVar.'='.$i;
            }
            return "http://".$_SERVER["HTTP_HOST"].$url;
                    
        
    }
    /*
    private function _jumpurl(){
        if(!empty($_SERVER["QUERY_STRING"])){//当前URL后面带参数的情况                        
            $url1=preg_replace("/(\&|\?)$this->pageVar\=[0-9a-zA-Z]+/", "", "?".$_SERVER["QUERY_STRING"]);
            if(empty($url1)){
                $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?{$this->pageVar}=";
            }else{
                $url1=  preg_replace("/^(\&|\?)/", "", $url1);
                $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?".$url1."&{$this->pageVar}=";
            }            
            return $url;
        }else{//不带参数的情况
            $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?{$this->pageVar}=";
            return $url;
        }
    } 
   */
}





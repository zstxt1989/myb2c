<?php
header("Content-Type:text/html;Charset=utf-8");
date_default_timezone_set('Asia/Shanghai');//时区设定
define('ROOT',str_replace(DIRECTORY_SEPARATOR,'/',substr(dirname(__FILE__),0,-7)));
include ROOT.'./configs/Configs.inc.php';
include ROOT.'./include/DB_MySql.class.php';
session_start();
//自动加载
function __autoload($classname){
    include ROOT.'./include/'.$classname.'.class.php';
}
spl_autoload_register("__autoload");//解决smarty与autoload冲突
//结束



//实例化数据库操作类
$db=new DB_MySql();
$db->connect($C["DbServer"], $C["DbUser"], $C["DbPw"], $C["DbName"]);
//结束

//加载smarty公共标签
include ROOT.'include/commonlibs.inc.php';
//结束

//加载帮助类
$help=new Help();
//结束
//加载数据工具类
$M=new M();
//结束
//request安全过滤
if(!get_magic_quotes_gpc()){
    foreach(array('_GET','_POST') as $request){
        foreach($$request as $k=>$v){
            $k{0}!='_'&& $$k=daddslashes($v);
        }
    }
}else{
    foreach(array('_GET','_POST') as $request){
        foreach($$request as $k=>$v){
            $k{0}!='_'&& $$k=$v;
        }
    }
}
function daddslashes($string){
    if(is_array($string)){
        foreach($string as $k=>$v){
            $string[$k]=daddslashes($v);
        }
    }else{
        $string = addslashes($string);
    }
    return $string;
}
//结束

/*
 * 读取用户session
 */
$U['uid']=$_SESSION['uid']?$_SESSION['uid']:0;
$U['name']=$_SESSION['name']?$_SESSION['name']:"";